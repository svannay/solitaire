import React, {Component} from 'react';

class Cell extends Component {
    render () {
        const { id, state, forbiden, selected} = this.props.bundle;
        let classes = '';
        if (forbiden) {
            classes = 'cell forbiden'
        } else {
            classes = 'cell' + (state ? ' active' : ' inactive') + (selected ? ' selected' : '');
        }
        return (
            <div 
                className={classes}
                onClick={() => this.props.onClick(id)}                    
            ></div>);
        
    }
}

export default Cell;