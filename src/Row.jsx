import React, {Component} from 'react';
import Cell from './Cell.jsx'

class Row extends Component {

    

    render () {
        
        const { width, row, forbiden, current} = this.props.bundle;
        const Cells = []

        for (let index = 0; index < width; index++) {
            const id = row * width + index;
            const bundle = { id: id, state: current.state[id], forbiden: forbiden[id], selected: current.selected[id] };
            Cells.push(<Cell key={`cell${id}`} onClick={this.props.onClick} bundle={bundle} />); 
        }

        return (
            <div className='row'>
                {Cells}
            </div>
        );
    }
}

export default Row;