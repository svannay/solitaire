import React, {Component} from 'react';
import './App.css';
import Row from './Row.jsx'
import Back from './Back.jsx'

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      width: 7,
      height: 7,
      grid: [],
      history: [{
        state: Array(7*7).fill(true),
        selected: Array(7*7).fill(false),
        currentPeg: null,
        isSelectedCell: false
      }],
      forbiden: Array(7*7).fill(false),
      stepNumber: 0,
      currentHole: null
      
      
    }
    /*for (let index = 0; index < this.state.width * this.state.height; index++) {
      this.state.grid.push({id: index, state: true, selected: false, forbiden:false});
    }*/
    this.state.forbiden[0] = true;
    this.state.forbiden[1] = true;
    this.state.forbiden[7] = true;

    this.state.forbiden[5] = true;
    this.state.forbiden[6] = true;
    this.state.forbiden[13] = true;
    
    this.state.forbiden[35] = true;
    this.state.forbiden[42] = true;
    this.state.forbiden[43] = true;

    this.state.forbiden[47] = true;
    this.state.forbiden[48] = true;
    this.state.forbiden[41] = true;

    for (let index = 0; index < this.state.history[0].state.length; index++) {
      this.state.forbiden[index] ? this.state.history[0].state[index] = false : this.state.history[0].state[index] = true;
    }
    this.state.history[0].state[17] = false;
  }

  handleClick = (id) => {
    //console.log(this.state.grid[id - 14].state);
    const history = [];
    let current = {};
    for (let i = 0; i < this.state.stepNumber + 1; i++) {
      history[i] = Object.assign({}, this.state.history[i]);
      history[i].selected = this.state.history[i].selected.slice();
      history[i].state = this.state.history[i].state.slice();
    }
    current = Object.assign(current, history[history.length - 1]);
    current.selected = history[history.length - 1].selected.slice();
    current.state = history[history.length - 1].state.slice();
    if (current.isSelectedCell) {
      if (current.selected[id]) {
        
        current.currentPeg = null;
        current.selected[id] = false;
        current.isSelectedCell = false;

        history[this.state.stepNumber] = Object.assign(history[this.state.stepNumber], current);

        this.setState({ history: history });
        
        //----
        console.log('//----');
        console.log(this.state.history);
        console.log('selected deselectionné');
      } else if (!current.state[id] && (id - current.currentPeg === 14 || id - current.currentPeg === -14 || 
                  id - current.currentPeg === 2 || id - current.currentPeg === -2)) {
        console.log('current peg : ' + current.currentPeg);
        console.log('id : ' + id);
    
        const middlePeg = ((id - current.currentPeg) / 2) + current.currentPeg;
        if (!current.state[middlePeg]) {
          return;
        }
        console.log('middle peg : ' + middlePeg);
        current.state[current.currentPeg] = false;
        current.state[middlePeg] = false;
        current.state[id] = true;
        current.selected[current.currentPeg] = false;
        current.isSelectedCell = false;
        current.currentPeg = null;
        const stepNumber = this.state.stepNumber + 1;
        
        this.setState({ history: history.concat([current]), stepNumber: stepNumber});
        //----
        console.log('//----');
        console.log(this.state.history);
        console.log('peg bougé');
      }
    } else {
      if (current.state[id] && !this.state.forbiden[id]) {
        let north = id - 14 >= 0 && !this.state.forbiden[id - 14] && !current.state[id - 14];
        let east = id % 7 + 2 < this.state.width && !this.state.forbiden[id + 2] && !current.state[id + 2];
        let west = id % 7 -2 >= 0 && !this.state.forbiden[id - 2] && !current.state[id - 2];
        let south = id + 14 < this.state.width * this.state.height && !this.state.forbiden[id + 14] && !current.state[id + 14];
        if (north || east || west || south) {
          current.currentPeg = id;
          current.selected[id] = true;
          current.isSelectedCell = true;

          history[this.state.stepNumber] = Object.assign(history[this.state.stepNumber], current);
          this.setState({ history: history });

          //----
          console.log('//----');
          console.log(this.state.history);
          console.log('selection');
          
        }
        
      //console.log(north + ' ' + east + ' ' + west + ' ' + south);
      }
    }
    console.log('//----');
    console.log('fin');



    
    
  }

  jumpTo(step) {
    this.setState({ stepNumber: step });
    console.log('step number : ' + this.state.stepNumber);
  }

  

  render () {
    //console.log(this.state.grid);
    const history = [];
    
    let current = {};
    for (let i = 0; i < this.state.history.length; i++) {
      history[i] = Object.assign({}, this.state.history[i]);
      history[i].selected = this.state.history[i].selected.slice();
      history[i].state = this.state.history[i].state.slice();
    }
    current = Object.assign(current, history[this.state.stepNumber]);
    current.selected = history[this.state.stepNumber].selected.slice();
    current.state = history[this.state.stepNumber].state.slice();
    console.log('/----------------------------/');
    console.log(this.state.history);
    console.log(current);
    console.log('/----------------------------/');

    const moves = history.map((step, move) => {
      const desc = move ?
        'Go to move #' + move :
        'Go to game start';
      return (
        <li key={move}>
          <button onClick={() => this.jumpTo(move)}>{desc}</button>
        </li>
      );
    });


    const grid = [];
    for (let index = 0; index < this.state.height; index++) {
      const bundle = {width: this.state.width, row: index, forbiden: this.state.forbiden, current: current};
      grid.push(<Row key={`row${index}`}  bundle={bundle} onClick={this.handleClick} />); 
    }

    

    return (
      <div className= 'conteneur'>
        <h1>Solitaire</h1>
        <div className='solitaire'>
          {grid}
        </div>
        <div className='historique'>
          <ul>{moves}</ul>
        </div>
      </div>
    );
  }
}

export default App;
